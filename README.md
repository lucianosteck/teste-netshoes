# Instruções Teste Netshoes

---

**Esse teste está rodando em um servidor estático Node. Para que ele rode da maneira correta, precisa instalar também o Nodemon.**

1. Caso **NÃO** tenha o Node instalado, vamos entrar no terminal e colocar: **npm install -g npm**

2. Já com o Node instalado em sua máquina, vamos instalar o Nodemon. No terminal vamos colocar: **npm install -g nodemon**

3. Depois de ter concluido, vamos rodar o servidor e para isso temos que estar na pasta raiz do projeto (onde tem o arquivo index.js). 
Ex: se o projeto foi copiado para o desktop, vamos colocar no terminal esse caminho: **cd desktop/teste-netshoes**

4. Depois disso, vamos rodar o servidor colocando no terminal: **nodemon index.js**

Obs: Nesse ponto o servidor estará rodando normalmente, e para acessar o projeto é só colocar no navegar o endereço: **http://localhost:7070/**