requirejs.config({
    paths: {
        jquery: 'jquery.min'
    }
});

require(['knockout', 'appViewModel'], function (ko, appViewModel) {
    ko.applyBindings(appViewModel);
    appViewModel.onLoad(appViewModel);
});