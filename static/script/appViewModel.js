define(['jquery', 'knockout'], function ($, ko) {
    "use strict";

    // O nome da chave no local storage
    var KEY_PRODUCTS_CART = "productsCart";

    return {
        gridProducts: ko.observableArray([]),
        productsCart: ko.observableArray([]),
        totalCart: ko.observable(0),
        cartVisible: ko.observable(false),
        installmentTotal: ko.observable(0),

        onLoad: function (self) {
            $.getJSON("http://localhost:7070/public/data/products.json", function (data) {
                self.gridProducts(data.products);
            });

            // preserva itens no carrinho caso tenha produtos adicionado
            var itemsCart = JSON.parse(localStorage.getItem(KEY_PRODUCTS_CART));
            if (itemsCart) {
                self.productsCart(itemsCart);
            }

            self.productsCart.subscribe(function () {
                localStorage.setItem(KEY_PRODUCTS_CART, ko.toJSON(self.productsCart()));
                self.totalPriceCart();
                self.calculateInstallmentTotal(self.totalCart());
            }, self);

            self.totalPriceCart();
            self.calculateInstallmentTotal(self.totalCart());
        },

        priceProduct: function (data) {
            return parseInt(data, 10);
        },

        centsProduct: function (data) {
            data = data.toFixed(2);
            var value = data.split('.')[1];

            if (value.toString().length == 1) {
                return ',' + value + '0';
            } else if (value.toString().length == 2) {
                return ',' + value;
            } else {
                return false;
            }
        },

        installments: function (price, installments) {
            var parcelas = 0;
            if (!installments == 0) {
                parcelas = (price / installments);
                parcelas = Math.floor(parcelas * 100) / 100;

                return parcelas.toFixed(2).replace('.', ',');
            } else {
                return false;
            }
        },

        addCart: function (data) {
            var itemsCart = localStorage.getItem(KEY_PRODUCTS_CART);

            if (itemsCart) {
                itemsCart = JSON.parse(itemsCart);                
                var ids = [];

                for (var i = 0; i < itemsCart.length; i++) {
                    ids.push(itemsCart[i].id);
                }

                if(ids.indexOf(data.id) != -1){  
                    console.log("Produto já adicionado na sacola");
                }else{
                    this.openCart(this);
                    $('#nav-icon').toggleClass('open');
                    this.productsCart.push(data);
                }
            }

            if(!itemsCart){
                this.productsCart.push(data);
            }
        },

        removeCart: function(data){
            var itemsCart = JSON.parse(localStorage.getItem(KEY_PRODUCTS_CART));
            var ids = [];

            for (var i = 0; i < itemsCart.length; i++) {
                ids.push(itemsCart[i].id);
            }

            if(ids.indexOf(data.id) != -1){  
                this.productsCart.splice(ids.indexOf(data.id), 1);
            }
        },

        calculateInstallmentTotal: function(price){
            var priceFloated = parseFloat(price.replace(",","."));
            var priceTemp = (priceFloated / this.calculateParcelas(priceFloated)).toFixed(2);
            
            this.installmentTotal('OU EM ATÉ ' + this.calculateParcelas(priceFloated) + 'X DE R$' + priceTemp.replace(".",","));
        },

        // calcula a quantidade de parcelas com parcelas mínima de 30 reais
        calculateParcelas: function (price) {
            var aux = (price / 30);
            
            if (aux > 10) {
                return 10;
            }else {
                return parseInt(aux);
            }
        },

        totalPriceCart: function(){
            var totalPrice = 0;
            for (var i = 0; i < this.productsCart().length; i++) {
                totalPrice = (totalPrice + this.productsCart()[i].price);
            }

            totalPrice = totalPrice.toFixed(2);
            
            this.totalCart(totalPrice.toString().replace('.', ','));
        },

        //função que evita fazer o scroll da página quando o menu e o minicart estiverem abertos
        stopBodyScrolling: function (bool) {
            if (bool === true) {
                $('body').css({overflow: 'hidden'});
            } else {
                $('body').css({overflow: 'initial'});
            }
        },

        openCart: function(data){
            data.cartVisible(true);
            data.stopBodyScrolling(true);
            
            $("#cart").stop().animate({right: '0px'}, 250);
            $("#sombra").stop().animate({left: '0px'}, 250);
            $("#cart").attr('tabindex', '0');
            $("#cart").focus();
            
            document.addEventListener('keydown', function(event) {
                if (event.keyCode == 27){
                    data.closeCart(data, event);
                }
            });
        },

        closeCart: function (data) {
            var larguraCart = $("#cart").width();
            var larguraSombra = $("#sombra").width();

            data.stopBodyScrolling(false);

            $("#cart").stop().animate({right: -larguraCart}, 250);
            $("#sombra").stop().animate({left: -larguraSombra}, 250);
            
            setTimeout(function () {
                data.cartVisible(false);
            }, 250);
        },
    };
});